# Front-end task

To run it use: 
python -c "import SimpleHTTPServer;SimpleHTTPServer.test()" 

# Possible improvements/roadmap:
1. Improve clouds drawing
2. Align rain/snow strength with actual weather info
3. Investigate flickr tags to get better pictures that represent city.
4. Check/fix browsers compatibility (For now "works on my machine" eg. Chrome :) )