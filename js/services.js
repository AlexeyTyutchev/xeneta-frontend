angular.module("weather").service("recentSearchesService", function ($log, RECENT_SEARCHES_TO_KEEP) {

    const RECENT_SEARCHES_STORAGE_KEY_NAME = "recentSearches";

    var recentSearches = [];

    this.storeSearch = function (searchString) {
        $log.debug("Adding %s to recent searches...", searchString);

        var storedSearch = recentSearches.find(function (s) {
            return s.searchString.toUpperCase() == searchString.toUpperCase();
        });

        if (storedSearch != null) {
            storedSearch.time = new Date().getTime();
            recentSearches = recentSearches.sort(function (el1, el2) {
                return el2.time - el1.time;
            });
        }
        else {
            if (recentSearches.length == RECENT_SEARCHES_TO_KEEP) {
                recentSearches.pop();
            }

            recentSearches.unshift({searchString: searchString, time: new Date().getTime()});
        }

        localStorage.setItem(RECENT_SEARCHES_STORAGE_KEY_NAME, JSON.stringify(recentSearches));
    };


    this.getRecentSearches = function () {
        return recentSearches;
    };


    var init = function() {
        $log.debug("Initializing recent searches service...");

        var item = localStorage.getItem(RECENT_SEARCHES_STORAGE_KEY_NAME);

        if (item != null) {
            recentSearches = JSON.parse(item);
        }

        $log.debug("%s recent searches loaded.", recentSearches.length);
    };

    init();
});


angular.module("weather").service("weatherService", function ($http) {
    var APPID = "0eeee407047a916863c6587e0c701711";
    var UNITS = "metric";
    var ACCURACY = "accurate";
    var baseUrl = "//api.openweathermap.org/data/2.5/weather";

    this.getWeather = function (city) {
        return $http.get(baseUrl, {params: {
            APPID: APPID,
            q: city,
            units: UNITS,
            type: ACCURACY
        }});
    }
});


angular.module("weather").service("flickrService", function ($http) {
    var me = this;

    var API_KEY = "10c39749519c6b925f384ffc505bbb19";
    var baseUrl= "https://api.flickr.com/services/rest/";

    this.doSearch = function(lat, lon) {
        var params = {
            api_key: API_KEY,
            per_page: 1,
            format: "json",
            tags: "poi, architecture, outdoor, outside",
            lat: lat,
            lon: lon,
            radius: 10,
            nojsoncallback: 1,
            page: 1,
            content_type: 1,
            sort: "interestingness-desc",
            method: "flickr.photos.search"
        };

        return $http.get(baseUrl, {params: params}).then(function (res) {
            if (res.data.photos.photo == null || res.data.photos.photo.length == 0) return null;
            else
                return "//farm" + res.data.photos.photo[0].farm + ".static.flickr.com/" + res.data.photos.photo[0].server + "/" +
                    res.data.photos.photo[0].id + "_" + res.data.photos.photo[0].secret + "_z.jpg";

        });
    };
});