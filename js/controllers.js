angular.module("weather").controller("WeatherController", function ($log, weatherService, recentSearchesService, flickrService) {
    var me = this;
    this.searchText = "";


    var refreshRecentSearches = function () {
        me.recentSearches = recentSearchesService.getRecentSearches();
    };


    var parseResult = function (res) {
        me.searchInfo = res.name + ", " + res.sys.country;

        flickrService.doSearch(res.coord.lat, res.coord.lon).then(function (imageRes) {
            $log.info("Image: %s", res);
            me.imageUrl = imageRes;


            var cloudiness = res.clouds.all;
            var multiplier = cloudiness < 20 ? 0 : (cloudiness < 80 ? 2 : 3);
            var array = new Array(Math.ceil(cloudiness / 10) * multiplier);
            array.fill(1);

            var weatherGroup = Math.floor(res.weather[0].id / 100);

            me.weather = {
                clouds: array,
                weatherGroup: weatherGroup,
                temperature: res.main.temp > 0 ? "+" + res.main.temp : res.main.temp,
                temperatureAboveZero: res.main.temp > 0,
                description: res.weather[0].description,
                icon: "//openweathermap.org/img/w/" + res.weather[0].icon + ".png",
                snowing: weatherGroup == 6,
                raining: weatherGroup > 1 && weatherGroup < 6
            };

            me.result = res;
        });
    };


    this.doSearch = function () {
        me.weather = null;
        me.searchInfo = null;

        var handleCityNotFound = function () {
            me.searchInfo = "City not found.";
        };

        weatherService.getWeather(me.searchText).then(
            function (res) {
                if (res.data.id != null) {
                    recentSearchesService.storeSearch(me.searchText);
                    refreshRecentSearches();
                    parseResult(res.data);
                }
                else handleCityNotFound();
            },
            function () {
                handleCityNotFound();
            }
        );
    };

    refreshRecentSearches();
});