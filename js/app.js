angular.module("weather", ["ui.bootstrap"]);

angular.module("weather").constant("RECENT_SEARCHES_TO_KEEP", 4);
angular.module("weather").constant("RAIN_DROPS_AMOUNT", 858);


angular.module("weather").directive("cloud", function ($log) {
    return {
        replace: true,
        template: "<div class='glyphicon glyphicon-cloud cloud'></span></div>",
        link: function($scope, el) {
            function generateColor() {
                return Math.random() < 0.5 ? "rgba(128, 128, 128, 0.5)" : "rgba(255, 255, 255, 0.7)"
            }

            
            var left = Math.round(Math.random() * (el.parent().width() - 100)) + "px";
            var top = Math.round(Math.random() * 150) + "px";
            el.css("left", left);
            el.css("top", top);
            el.css("color", generateColor());
            el.css("fontSize", Math.round(Math.random() * 200 + 170));
        }
    }
});

angular.module("weather").directive("rain", function (RAIN_DROPS_AMOUNT) {
    return {
        link: function($scope, el) {
            function randRange(minNum, maxNum) {
                return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
            }

            for( i = 1; i < RAIN_DROPS_AMOUNT; i++) {
                var dropLeft = randRange(0,1600);
                var dropTop = randRange(-1000,1400);

                el.append('<div class="rain-drop" style="left: ' + dropLeft + 'px; top: ' + dropTop + 'px"></div>');
            }
        }
    }
});